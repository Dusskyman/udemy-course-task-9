import 'package:flutter/material.dart';
import 'package:udemy_task_9/custom_widgets/custombutton.dart';
import 'package:udemy_task_9/custom_widgets/some_cool_continers.dart';
import 'package:udemy_task_9/custom_widgets/some_column.dart';
import 'package:udemy_task_9/custom_widgets/some_row.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
      onGenerateRoute: (settings) {
        if (settings.name == CoolColumn.route) {
          return MaterialPageRoute(builder: (ctx) => CoolColumn());
        } else if (settings.name == CoolRow.route) {
          return MaterialPageRoute(builder: (ctx) => CoolRow());
        } else if (settings.name == StackContainer.route) {
          return MaterialPageRoute(builder: (ctx) => StackContainer());
        }
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ColoredBox(
          color: Colors.blueGrey,
          child: Padding(
            padding: EdgeInsets.only(right: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomButton(
                  routeTo: CoolRow.route,
                ),
                CustomButton(
                  routeTo: CoolColumn.route,
                ),
                CustomButton(
                  routeTo: StackContainer.route,
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Text(
          'Just screen with drawer',
          style: TextStyle(fontSize: 40),
        ),
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.red[300],
                Colors.red[900],
              ]),
        ),
      ),
    );
  }
}
