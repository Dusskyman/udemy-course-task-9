import 'package:flutter/material.dart';

class StackContainer extends StatelessWidget {
  static const String route = '/cool-containers';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 200,
              height: 200,
              color: Colors.amber[900],
            ),
            Container(
              width: 150,
              height: 150,
              color: Colors.amber[700],
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.amber[500],
            ),
            Container(
              width: 50,
              height: 50,
              color: Colors.amber[300],
            ),
          ],
        ),
      ),
    );
  }
}
