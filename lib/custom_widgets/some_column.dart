import 'package:flutter/material.dart';

class CoolColumn extends StatelessWidget {
  static const String route = '/cool-column';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 100,
            height: 100,
            color: Colors.amber,
          ),
          Divider(
            color: Colors.black,
            thickness: 5,
            indent: 10,
            endIndent: 10,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.amber,
          ),
          Divider(
            color: Colors.black,
            thickness: 5,
            indent: 10,
            endIndent: 10,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.amber,
          ),
        ],
      ),
    );
  }
}
