import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String routeTo;
  CustomButton({
    this.routeTo,
  });
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: double.infinity,
      child: Material(
        color: Colors.lightGreenAccent,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
        child: InkWell(
          highlightColor: Colors.amberAccent[700],
          splashColor: Colors.blue,
          onTap: () {
            Navigator.pushNamed(context, routeTo);
          },
          child: Center(
            child: Text(
              'Route to $routeTo',
              style: TextStyle(fontSize: 30),
            ),
          ),
        ),
      ),
    );
  }
}
